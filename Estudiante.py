from plantas import Plantas
class estudiante():
    def __init__(self,nombre):
        self._nombre= nombre
        self._plantas= []
        pass

    @property
    def nombre(self):
        return self._nombre
    @nombre.setter
    def nombre(self, nombre):
        if isinstance(nombre, str):
            self._nombre = nombre
        else:
            print("El tipo de dato no corresponde")

    @property
    def plantas(self):
        return self._plantas
    @plantas.setter
    def plantas(self, plantas):
        if isinstance(plantas, Plantas):
            self._plantas.append(plantas)