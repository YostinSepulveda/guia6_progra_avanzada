import random
import os
import time
from Estudiante import estudiante 
from plantas import Plantas

def main():

    ListaPlantas=[Plantas("Violeta"),Plantas("Rabano"),Plantas("Trebol"),Plantas("Hierba")]

    ListaEstudiantes= [estudiante("Alicia"),estudiante("Andres"),estudiante("Belen"),estudiante("David"),estudiante("Eva")
    ,estudiante("Jose"),estudiante("Larry"),estudiante("Lucia"),estudiante("Marit"),estudiante("Pepito"),estudiante("Rocio")
    ,estudiante("Sergio")]
    
    ListaFinal1=[]
    ListaFinal2=[]

    
    for i in ListaEstudiantes:
        for x in range(4):
            i.plantas.append(random.choice(ListaPlantas))
    c=0
    for i in ListaEstudiantes:
        for x in i.plantas:
            if c in {0,1,4,5,8,9,12,13,16,17,20,21,24,25,28,29,32,33,36,37,40,41,44,45,48}:
                if (x.nombre=="Hierba"):
                    ListaFinal1.append("H")
                if (x.nombre=="Violeta"):
                    ListaFinal1.append("V")
                if (x.nombre=="Rabano"):
                    ListaFinal1.append("R")
                if (x.nombre=="Trebol"):
                    ListaFinal1.append("T")
            else:
                if (x.nombre=="Hierba"):
                    ListaFinal2.append("H")
                if (x.nombre=="Violeta"):
                    ListaFinal2.append("V")
                if (x.nombre=="Rabano"):
                    ListaFinal2.append("R")
                if (x.nombre=="Trebol"):
                    ListaFinal2.append("T")
            c=c+1
    os.system("clear")
    print("Repartiendo las semillas...")
    time.sleep(1.5)
    os.system("clear")
    
    print("\t[Ventana][Ventana][Ventana]\n","\t ",*ListaFinal1,sep="")
    print("\t ",*ListaFinal2,sep="")
    
    menuop=0
    while menuop==0:
        print("Para saber que plantita le toco a cada niño\n \t Elige una de las opciones!\n")
        print("1.Alicia 2.Andres 3.Belen 4.David 5.Eva 6.Jorge\n7.Larry 8.Lucia 9.Marit 10.Pepito 11.Rocio 12.Sergio 13.Salir")
        niño= input("\tPresiona un numero y dale a enter!\n")
        if niño=="1":
            os.system("clear")
            print("\t[Ventana][Ventana][Ventana]\n","\t ",*ListaFinal1,sep="")
            print("\t ",*ListaFinal2,sep="")
            print("Alicia tiene semillas de",ListaEstudiantes[0].plantas[0].nombre,end=", ")
            print(ListaEstudiantes[0].plantas[1].nombre,end=", ")
            print(ListaEstudiantes[0].plantas[2].nombre,end=" y ")
            print(ListaEstudiantes[0].plantas[3].nombre)
            opcion= input("Ingresa 1 para volver al menú, ingresa cualquier otra tecla para salir del programa\n")
            if opcion=="1":
                os.system("clear")
                print("\t[Ventana][Ventana][Ventana]\n","\t ",*ListaFinal1,sep="")
                print("\t ",*ListaFinal2,sep="")
                pass
            else:
                os.system("clear")
                print("\n\n\n\t\tAdios!!")
                time.sleep(1)
                os.system("clear")
                menuop=1

        elif niño=="2":
            os.system("clear")
            print("\t[Ventana][Ventana][Ventana]\n","\t ",*ListaFinal1,sep="")
            print("\t ",*ListaFinal2,sep="")
            print("Andres tiene semillas de",ListaEstudiantes[1].plantas[0].nombre,end=", ")
            print(ListaEstudiantes[1].plantas[1].nombre,end=", ")
            print(ListaEstudiantes[1].plantas[2].nombre,end=" y ")
            print(ListaEstudiantes[1].plantas[3].nombre)
            opcion= input("Ingresa 1 para volver al menú, ingresa cualquier otra tecla para salir del programa\n")
            if opcion=="1":
                os.system("clear")
                print("\t[Ventana][Ventana][Ventana]\n","\t ",*ListaFinal1,sep="")
                print("\t ",*ListaFinal2,sep="")
                pass
            else:
                os.system("clear")
                print("\n\n\n\t\tAdios!!")
                time.sleep(1)
                os.system("clear")
                menuop=1
        elif niño=="3":
            
            os.system("clear")
            print("\t[Ventana][Ventana][Ventana]\n","\t ",*ListaFinal1,sep="")
            print("\t ",*ListaFinal2,sep="")
            print("Belen tiene semillas de",ListaEstudiantes[2].plantas[0].nombre,end=", ")
            print(ListaEstudiantes[2].plantas[1].nombre,end=", ")
            print(ListaEstudiantes[2].plantas[2].nombre,end=" y ")
            print(ListaEstudiantes[2].plantas[3].nombre)
            opcion= input("Ingresa 1 para volver al menú, ingresa cualquier otra tecla para salir del programa\n")
            if opcion=="1":
                os.system("clear")
                print("\t[Ventana][Ventana][Ventana]\n","\t ",*ListaFinal1,sep="")
                print("\t ",*ListaFinal2,sep="")
                pass
            else:
                os.system("clear")
                print("\n\n\n\t\tAdios!!")
                time.sleep(1)
                os.system("clear")
                menuop=1
        elif niño=="4":
            
            os.system("clear")
            print("\t[Ventana][Ventana][Ventana]\n","\t ",*ListaFinal1,sep="")
            print("\t ",*ListaFinal2,sep="")
            print("David tiene semillas de",ListaEstudiantes[3].plantas[0].nombre,end=", ")
            print(ListaEstudiantes[3].plantas[1].nombre,end=", ")
            print(ListaEstudiantes[3].plantas[2].nombre,end=" y ")
            print(ListaEstudiantes[3].plantas[3].nombre)
            opcion= input("Ingresa 1 para volver al menú, ingresa cualquier otra tecla para salir del programa\n")
            if opcion=="1":
                os.system("clear")
                print("\t[Ventana][Ventana][Ventana]\n","\t ",*ListaFinal1,sep="")
                print("\t ",*ListaFinal2,sep="")
                pass
            else:
                os.system("clear")
                print("\n\n\n\t\tAdios!!")
                time.sleep(1)
                os.system("clear")
                menuop=1
        elif niño=="5":
            
            os.system("clear")
            print("\t[Ventana][Ventana][Ventana]\n","\t ",*ListaFinal1,sep="")
            print("\t ",*ListaFinal2,sep="")
            print("Eva tiene semillas de",ListaEstudiantes[4].plantas[0].nombre,end=", ")
            print(ListaEstudiantes[4].plantas[1].nombre,end=", ")
            print(ListaEstudiantes[4].plantas[2].nombre,end=" y ")
            print(ListaEstudiantes[4].plantas[3].nombre)
            opcion= input("Ingresa 1 para volver al menú, ingresa cualquier otra tecla para salir del programa\n")
            if opcion=="1":
                os.system("clear")
                print("\t[Ventana][Ventana][Ventana]\n","\t ",*ListaFinal1,sep="")
                print("\t ",*ListaFinal2,sep="")
                pass
            else:
                os.system("clear")
                print("\n\n\n\t\tAdios!!")
                time.sleep(1)
                os.system("clear")
                menuop=1          
        elif niño=="6":
            
            os.system("clear")
            print("\t[Ventana][Ventana][Ventana]\n","\t ",*ListaFinal1,sep="")
            print("\t ",*ListaFinal2,sep="")
            print("Jorge tiene semillas de",ListaEstudiantes[5].plantas[0].nombre,end=", ")
            print(ListaEstudiantes[5].plantas[1].nombre,end=", ")
            print(ListaEstudiantes[5].plantas[2].nombre,end=" y ")
            print(ListaEstudiantes[5].plantas[3].nombre)
            opcion= input("Ingresa 1 para volver al menú, ingresa cualquier otra tecla para salir del programa\n")
            if opcion=="1":
                os.system("clear")
                print("\t[Ventana][Ventana][Ventana]\n","\t ",*ListaFinal1,sep="")
                print("\t ",*ListaFinal2,sep="")
                pass
            else:
                os.system("clear")
                print("\n\n\n\t\tAdios!!")
                time.sleep(1)
                os.system("clear")
                menuop=1            
        elif niño=="7":
            
            os.system("clear")
            print("\t[Ventana][Ventana][Ventana]\n","\t ",*ListaFinal1,sep="")
            print("\t ",*ListaFinal2,sep="")
            print("Larry tiene semillas de",ListaEstudiantes[6].plantas[0].nombre,end=", ")
            print(ListaEstudiantes[6].plantas[1].nombre,end=", ")
            print(ListaEstudiantes[6].plantas[2].nombre,end=" y ")
            print(ListaEstudiantes[6].plantas[3].nombre)
            opcion= input("Ingresa 1 para volver al menú, ingresa cualquier otra tecla para salir del programa\n")
            if opcion=="1":
                os.system("clear")
                print("\t[Ventana][Ventana][Ventana]\n","\t ",*ListaFinal1,sep="")
                print("\t ",*ListaFinal2,sep="")
                pass
            else:
                os.system("clear")
                print("\n\n\n\t\tAdios!!")
                time.sleep(1)
                os.system("clear")
                menuop=1            
        elif niño=="8":
            
            os.system("clear")
            print("\t[Ventana][Ventana][Ventana]\n","\t ",*ListaFinal1,sep="")
            print("\t ",*ListaFinal2,sep="")
            print("Lucia tiene semillas de",ListaEstudiantes[7].plantas[0].nombre,end=", ")
            print(ListaEstudiantes[7].plantas[1].nombre,end=", ")
            print(ListaEstudiantes[7].plantas[2].nombre,end=" y ")
            print(ListaEstudiantes[7].plantas[3].nombre)
            opcion= input("Ingresa 1 para volver al menú, ingresa cualquier otra tecla para salir del programa\n")
            if opcion=="1":
                os.system("clear")
                print("\t[Ventana][Ventana][Ventana]\n","\t ",*ListaFinal1,sep="")
                print("\t ",*ListaFinal2,sep="")
                pass
            else:
                os.system("clear")
                print("\n\n\n\t\tAdios!!")
                time.sleep(1)
                os.system("clear")
                menuop=1 
                           
        elif niño=="9":
            os.system("clear")
            print("\t[Ventana][Ventana][Ventana]\n","\t ",*ListaFinal1,sep="")
            print("\t ",*ListaFinal2,sep="")
            print("Marit tiene semillas de",ListaEstudiantes[8].plantas[0].nombre,end=", ")
            print(ListaEstudiantes[8].plantas[1].nombre,end=", ")
            print(ListaEstudiantes[8].plantas[2].nombre,end=" y ")
            print(ListaEstudiantes[8].plantas[3].nombre)
            opcion= input("Ingresa 1 para volver al menú, ingresa cualquier otra tecla para salir del programa\n")
            if opcion=="1":
                os.system("clear")
                print("\t[Ventana][Ventana][Ventana]\n","\t ",*ListaFinal1,sep="")
                print("\t ",*ListaFinal2,sep="")
                pass
            else:
                os.system("clear")
                print("\n\n\n\t\tAdios!!")
                time.sleep(1)
                os.system("clear")
                menuop=1
                            
        elif niño=="10":
            os.system("clear")
            print("\t[Ventana][Ventana][Ventana]\n","\t ",*ListaFinal1,sep="")
            print("\t ",*ListaFinal2,sep="")
            print("Pepito tiene semillas de",ListaEstudiantes[9].plantas[0].nombre,end=", ")
            print(ListaEstudiantes[9].plantas[1].nombre,end=", ")
            print(ListaEstudiantes[9].plantas[2].nombre,end=" y ")
            print(ListaEstudiantes[9].plantas[3].nombre)
            opcion= input("Ingresa 1 para volver al menú, ingresa cualquier otra tecla para salir del programa\n")
            if opcion=="1":
                os.system("clear")
                print("\t[Ventana][Ventana][Ventana]\n","\t ",*ListaFinal1,sep="")
                print("\t ",*ListaFinal2,sep="")
                pass
            else:
                os.system("clear")
                print("\n\n\n\t\tAdios!!")
                time.sleep(1)
                os.system("clear")
                menuop=1
                            
        elif niño=="11":
            os.system("clear")
            print("\t[Ventana][Ventana][Ventana]\n","\t ",*ListaFinal1,sep="")
            print("\t ",*ListaFinal2,sep="")
            print("Rocio tiene semillas de",ListaEstudiantes[10].plantas[0].nombre,end=", ")
            print(ListaEstudiantes[10].plantas[1].nombre,end=", ")
            print(ListaEstudiantes[10].plantas[2].nombre,end=" y ")
            print(ListaEstudiantes[10].plantas[3].nombre)
            opcion= input("Ingresa 1 para volver al menú, ingresa cualquier otra tecla para salir del programa\n")
            if opcion=="1":
                os.system("clear")
                print("\t[Ventana][Ventana][Ventana]\n","\t ",*ListaFinal1,sep="")
                print("\t ",*ListaFinal2,sep="")
                pass
            else:
                os.system("clear")
                print("\n\n\n\t\tAdios!!")
                time.sleep(1)
                os.system("clear")
                menuop=1
                            
        elif niño=="12":
            os.system("clear")
            print("\t[Ventana][Ventana][Ventana]\n","\t ",*ListaFinal1,sep="")
            print("\t ",*ListaFinal2,sep="")
            print("Sergio tiene semillas de",ListaEstudiantes[11].plantas[0].nombre,end=", ")
            print(ListaEstudiantes[11].plantas[1].nombre,end=", ")
            print(ListaEstudiantes[11].plantas[2].nombre,end=" y ")
            print(ListaEstudiantes[11].plantas[3].nombre)
            opcion= input("Ingresa 1 para volver al menú, ingresa cualquier otra tecla para salir del programa\n")
            if opcion=="1":
                os.system("clear")
                print("\t[Ventana][Ventana][Ventana]\n","\t ",*ListaFinal1,sep="")
                print("\t ",*ListaFinal2,sep="")
                pass
            else:
                os.system("clear")
                print("\n\n\n\t\tAdios!!")
                time.sleep(1)
                os.system("clear")
                menuop=1
                
        elif niño=="13":
            os.system("clear")
            print("\n\n\n\t\tAdios!!")
            time.sleep(1)
            os.system("clear")
            menuop=1          
        else:
            os.system("clear")
            print("\t[Ventana][Ventana][Ventana]\n","\t ",*ListaFinal1,sep="")
            print("\t ",*ListaFinal2,sep="")

            print("La opción ingresada no es valida, intenta nuevamente!!\n")  
  
            
            
if __name__ == '__main__':
    main()







